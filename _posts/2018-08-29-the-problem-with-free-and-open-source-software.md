---
layout: post
title: The Problem with Free &amp; Open Source Software
author: James Thompson
date: '2018-08-29 00:00:00 +0000'
category: 'Open Source'
summary: Freedom has consequences. Sometimes those consequences are not to our liking, and still other times we have no
    control over the consequences.
thumbnail: 'posts/the-problem-with-free-and-open-source-software.jpg'
---

Back in the early 2000's I got into a discussion about the relative merits and problems with Free and Open Source
software. One of the points of discussion was the first freedom identified by Free Software:

> The freedom to run the program as you wish, for any purpose

This same freedom is identified in article five of the Open Source Definition. The point of that discussion was about
the moral position of free and open source software as it related to proprietary software. My counterpart proposed that
any software that was not free or open source was morally evil, while I defended the rights of creators to determine the
bounds of their software's use. I still stand by my position and will not retread it here. Any software developer has
the right to define how and by whom their software can be used. But, I also believe that free and open source software
is superior to proprietary software for many reasons and in many circumstances. The most important of those reasons is
freedom.

## Freedom Is Not Free

Freedom has consequences. Taking action necessitates accepting the consequences of such action. Sometimes those
consequences are not to our liking, and still other times we have no control over the consequences. The trouble arises
when the consequences become so objectionable that we think the problem is with freedom itself. That's the trap that is
always lurking just beyond our view, tempting us to restrict freedom for the sake of our comfort. But, the truth is that
to gain our comfort we have to give up our freedom entirely.

## Denying Freedom

Today I ran across this tweet about a project I had never heard about doing something they have every right to do, but
about which they were thoroughly ignorant of the full consequences:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Lerna, a popular tool for managing JavaScript
projects with multiple packages, has added a clause to its MIT license blocking a variety of companies from using future
versions: <a href="https://t.co/ikypwrT84G">https://t.co/ikypwrT84G</a> – what do you think about this well-meaning
moral stance? <a href="https://t.co/GF1PGBpEcP">pic.twitter.com/GF1PGBpEcP</a></p>&mdash; JavaScript Daily
(@JavaScriptDaily) <a href="https://twitter.com/JavaScriptDaily/status/1034842426861645824?ref_src=twsrc%5Etfw">August
29, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

The Lerna project was originally licensed under one of the most permissive open source licenses there is: the MIT
license. But, they decided to add an additional clause:

The following license shall not be granted to the following entities or any subsidiary thereof due to their
collaboration with US Immigration and Customs Enforcement ("ICE"):

The specific list of companies is immaterial, as are any political opinions concerning US Immigration and Customs
Enforcement policies. The consequence of this change was that Lerna has made their project untouchable to the rest of
the Free and Open Source software community. By denying the freedom for any person or group to use their project they
have made their project incompatible with any project that upholds either the Free Software or the Open Source
definitions.

This is not the first time Free or Open Source software has dealt with the moral questions. Back in 2012 it was called
out the license used on JSON.org contained an explicit morality clause. In fact, the ambiguous clause is still included
in the license. The Debian project has labelled this license as non-free because it imposes this additional clause.

The Open Source Initiative is clear in their FAQ that restricting a person or group is not permissible when they answer
the question "Can I stop "evil people" from using my program?"

No. The Open Source Definition specifies that Open Source licenses may not discriminate against persons or groups.
Giving everyone freedom means giving evil people freedom, too.

## Unintended Consequences

The reason both the Debian project and the Open Source Initiative are so unequivocal is that such restrictions cannot be
reliably bounded. Debian recognized that the additional clause in the JSON license would pollute their project and
require all its users to abide by the terms, which is something they could not reliably even enforce. Adopting
restrictions on freedom always ripple out and effect others.

Moral and ethical decisions exist within cultural contexts, and that makes them practically impossible to reliably
define ahead of time. Since neither the Free Software or Open Source movements were omniscient they erred on the side of
freedom. But, the maintainers of the Lerna project, the original JSON implementors, and others will continue to repeat
the hubris of thinking they can adequately respond to evil by denying freedom not only to their perceived enemies, but
also to everyone else who wants to use or produce Free and Open Source software that preserves freedom.

## Freedom is the Problem (and the solution)

At the end of the day freedom is the problem. It has costs, and consequences. But, freedom is worth having, even in the
realm of software. Freedom means that with the same tools some would employ for evil we have all the rights to employ
those same tools for good. But, we can't maintain the freedom to do good effectively without risking freedom for others
to do evil. That is the risk, but, in this case, I think it is a risk worth taking.
