---
layout: post
title: Overtime Hurts Your Software &amp; Your Team
author: James Thompson
date: '2018-09-29 00:00:00 +0000'
category: teams
summary: Overtime reveals how much many organizations lack professionalism, clear priorities, and agility. And, it hurts
    their products and their people.
thumbnail: 'posts/overtime-hurts-your-software-and-your-team.jpg'
---

At almost every company there is some level of pressure to work overtime. It gets couched in language about mission,
impact, hustle, and other euphemisms. Positive employee recognition usually comes to people willing to work the extra
hours.

> "Joe really went that extra mile to help us meet our goal. Staying late, coming in on weekends. He really sacrificed
> for our mission."

Thankfully, I've never worked anywhere where my aversion to overtime was denigrated. But, praising overtime is
ridiculous. Overtime is a sign of problems that should never receive praise. Ever.

At its most basic the need for overtime reflects a breakdown in professionalism, priorities, and agility. I'm primarily
concerned with overtime for software development organizations. But, the negative effects it has on productivity and
quality can be observed in any context.

## Professionalism

Professionalism comes down to discipline. Competence and skill are obtained through discipline, no matter the endeavor.
Overtime represents a lack of discipline with relation to scheduling. It represents a lack of discipline in other areas
too. But, fundamentally, it is a failure to confine our work in a way that prevents it from pushing out other things.

The phrase "work hard, play hard" is often associated with the mindset that leads to overtime. The idea being that if we
push ourselves to work hard towards a goal, when we achieve it we gain license to let loose. But, what about when you
miss the mark? What about when you never get the break because another goal arises? When overtime is acceptable it
breeds more overtime to the exclusion of ever being able to play.

A better approach is embodied in the phrase "work hard, then go home." This captures an assumption of balance between
work and the rest of life. Each day we endeavor to do the work we need to and when that day is over we go home. What we
do when we are done with a day's work is our concern. We have permission to leave work at work and take ownership of
our time.

This approach puts the power back in the hands of the individual to determine what is important to them. Some may say
they want to prioritize their work, but we will get to other reasons that is often an unwise trade. Professionalism
means respecting boundaries and exercising discipline. We should not expect people to choose between career and family,
or job and friends, or work and fun. Professionals, and the organizations that want the best of them, should be
disciplined about this balance.

## Priorities

Another area that is closely related to professionalism is that of priorities. In almost every case where I've observed,
or been asked, to accept overtime has come when someone messed up what the most important thing was. Somewhere, somehow,
prioritization had failed. The most important and urgent work was not done at the most appropriate time. Sometimes
mistakes happen or circumstances change. But, more often than not, someone got the priorities wrong.

This comes back to the need for communication. We need to be seeking consistent and clear feedback as we work. Anytime
that breaks down we increase the risk we will not be working on the most important things when we need to. The reality
is that we should not work on things whose value is suspect. Work to clarify our lack of understanding can help us keep
the most valuable thing at the fore. But, if we can't validate something's value then it is not likely something worth
working on. Failure to prioritize appropriately risks the product's success since we can't be sure we're building what
matters.

When priorities are properly understood, along with their value it becomes easier to adjust plans. Understanding the
value of our work provides opportunities to manage scope and timelines.We can move valuable work to the front and either
delay or drop less valuable work. Emphasizing the important of priorities allows us to get at one of the root causes of
needing overtime and avoid it.

## After-Hours

I've worked as a software engineer for the vast majority of the last fifteen years. But, interspersed in that time, I
have also done a lot of work maintaining business computer systems. Sometimes those systems go down and require
after-hours attention. Re-aligning our schedules can be part of the job we have sometimes. But, to connect back to
professionalism, that should not translate into working a full day plus giving up our personal time.

I've been fortunate to have roles at companies that permitted me to flex my schedule to accommodate the unexpected. If
I'm fixing a server at 2:00am, I was not expected to be back in office for a normal day the following morning. Routine
duties would have to be rescheduled to compensate for my need to rest and avoid burn-out. It's important to recognize
that adjusting one's schedule or accommodating after-hours work is not the same as being pushed into, or even
voluntarily accepting overtime.

## Agility

The first value statement of [the Manifesto for Agile Software Development][agile-manifesto] is "Individuals and
interactions over processes and tools." People come first in any agile organization. You can not take care of the work
you want to get done unless you first take care of the people you expect to do that work. And, among [the principles
underlying that same manifesto][agile-principles] is that of Sustainable Pace.

> Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a
> constant pace indefinitely.

Overtime is an adversary to Sustainable Pace. If we need overtime then something about our processes is misaligned, or
not working. In an agile organization overtime is a symptom of more systemic failings. Address the problems in
prioritization, scoping, quality or other areas. Whatever the root cause is, address that. Don't accept overtime as
unavoidable, or necessary.

Agility requires keeping the lives of the people involved balanced. This means they need time to decompress. We can not
work effectively if we are always working. Eventually our ability to deliver will suffer, quality will suffer, and we
will be called upon to invest more overtime to fix what we failed to do well the first time. So, if we can focus on
fixing the systemic issues that make overtime seem necessary, then we can avoid overtime and be more consistently at our
best.

## Productivity

Research also indicates that [overtime is a waste of time][productivity]. Productivity drops as sustained overtime
continues. And, it appears that the drop can entirely negate the extra time. The emerging wisdom indicates that when we
stay late we don't get more done, we get the same done more slowly. Overtime work is, as my title points out
unproductive time.

Overtime has a negative effect on productivity for numerous reasons. That makes it better to avoid altogether. Why spend
more unproductive time working if we could simply rest and return to full productivity after some time away? There is no
good justification. We have conditioned ourselves to think overtime is somehow normal. We have engaged in self-deception
in spite of what research and even intuition can tell us.

## Quality

Finally, there is the matter of quality. Overtime does not encourage continued discipline around good practices for
maintaining consistent quality. Overtime is, itself, a way to cut corners, and it encourages more of the same in the
work done during that time. If we are working overtime then we have already been stripped of the ability to take our
time and to be thoughtful with regards to the things we build.

When we lose encouragement to be thoughtful and disciplined in our development practices our quality slips. We skip a
test here and there because that part of the solution is simple enough. We become arrogant in our ability to build
software without the practices that sustain quality over time. And, our arrogance is always misplaced. None of us is as
smart as we think we are. The practices and discipline that help us maintain quality software are our best safeguards
against that arrogance run amok. But overtime does not afford us the one thing we need to maintain those practices and
discipline: time.

Quality will always suffer in the face of overtime. Maybe not immediately. But, as it continues to be accepted or
encouraged, it will erode sound practices and stifle even the best developer's discipline. If we want to maintain high
quality software and high quality teams we can not accept overtime as normal. It never pays off in the way it promises
and we often can't know that until the costs have gotten much higher.

---

So, how do we address the problem? Personally, I don't work overtime. I get annoyed when I see folks praised for working
overtime. I also speak up for others, whether they want me to or not, who often don't know any better. So, we have to
start by recognizing and calling out overtime as a problem. That is the first step.

Overtime is a sign of dysfunction. It means something, somewhere, has gone wrong. If someone works overtime we should
identify how to make sure that never happens again. We never want overtime to turn into burn-out. But, when we praise
overtime we are inviting burn-out to come along too. This needs to be something an organizations draws a line in the
cement over.

How have you tried to combat the impulse or calls for overtime? Have you found other ways to achieve what is needed
without sacrificing personal time? How can we all do better at ensuring work does not get to take over our lives?

[agile-manifesto]: https://agilemanifesto.org/
[agile-principles]: https://agilemanifesto.org/principles.html
[productivity]: https://www.entrepreneur.com/article/288359