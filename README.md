# james.thomps.onl

This project is based on the work of [Sujay Kundu][sujaykundul] and the [Devlopr][devlopr] Jekyll theme. All the
original source code, and modifications to it are licensed under an [MIT license](LICENSE.txt). The content is licensed
under a [Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)][cc-by-sa-4.0] license by
[James Thompson][plainprogrammer]. 

[sujaykundul]: https://github.com/sujaykundu777/
[devlopr]: https://github.com/sujaykundu777/devlopr-jekyll/
[cc-by-sa-4.0]: https://creativecommons.org/licenses/by-sa/4.0/
[plainprogrammer]: https://james.thomps.onl/
